/**
 * 本ファイルまでの相対パス
 */
var PATH = (function () {
	var scripts = document.getElementsByTagName('script');
	var src = scripts[scripts.length - 1].getAttribute('src');
	var index = src.lastIndexOf('/');
	return (index !== -1) ? src.substring(0, index) : '';
})();

/**
 * ブレークポイント
 */
var BREAKPOINT = 640;

/**
 * IE判別
 */

var ua = navigator.userAgent;
var isIE = ua.match(/msie/i),
	isIE8 = ua.match(/msie [8.]/i),
	isIE9 = ua.match(/msie [9.]/i),
	isIE10 = ua.match(/msie [10.]/i);
	isIE11 = ua.match(/msie [11.]/i);
if (isIE) {
	$("body").addClass('ie');
	if (isIE8) {
		$("body").addClass('ie8');
	} else if (isIE9) {
		$("body").addClass('ie9');
	} else if (isIE10) {
		$("body").addClass('ie10');
	} else if (isIE11) {
		$("body").addClass('ie11');
	}
}


/**
 * ページ内のスムーズスクロール
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 * @require jQuery Easing v1.3
 */
function setSmoothScroll($) {
// setSmoothScroll という名の関数を定義
	$('a[href^=#]').click(function() {
      // スクロールの速度
      var speed = 400; // ミリ秒で記述
      var href= $(this).attr("href");
      var target = $(href == "#" || href == "" ? 'html' : href);
      var position = target.offset().top;
      $('body,html').animate({scrollTop:position}, speed, 'swing');
      return false;
   });
}

/**
 * リンクの別ウィンドウ表示
 *
 * @param jQuery $ jQuery オブジェクト
 * @require jQuery v1.7.2
 */
function setLinkNewWindow($) {
	var $targets = $('a[href^="http://"], a[href^="https://"]').not('a[href^="http://'+ location.hostname +'"], a[href^="https://'+ location.hostname +'"], a[data-rel="external"]');
	$targets.on('click', function() {
		open($(this).attr('href'), null);
		return false;
	});
}

/**
 * 読み込み完了時の処理
 *
 * @require jQuery v1.7.2
 */
jQuery(function($) {
	setSmoothScroll($);
	setLinkNewWindow($);
});